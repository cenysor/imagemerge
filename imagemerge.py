#! /usr/bin/python3

# A program to combine multiple images

import os
import random
from PIL import Image
import sys

while True:
    try:
        print('Enter image path!')
        image1 = input()
        new1 = Image.open(image1)
        break
    except FileNotFoundError:
        print('No such file or directory.')
        continue

while True:
    while True:
        try:
            print('Enter another image path to merge or [exit]!')
            image2 = input()

            if image2 == 'exit':
                sys.exit()

            else:
                new2 = Image.open(image2)
                break
        except FileNotFoundError:
            print('No such file or directory.')
            continue

    width, height = new2.size

    new3 = new1.resize((width, height))

    for x in range(0, width, random.randint(2, 4)):
        for y in range(0, height, random.randint(2, 4)):
            pixel = new2.getpixel((x, y))
            new3.putpixel((x, y), pixel)

    while True:
        try:
            new3.save(save)
            print('Image has been saved.')
            break
        except NameError:
            print('Enter [path/filename.extension] to save file!')
            save = input()
            continue
        except KeyError:
            print('Invalid extension.')
            print('Enter [path/filename.extension] to save file!')
            save = input()
            continue

    new1 = Image.open(save)